# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html

from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from scrapy.exceptions import DropItem
from dooms.competitions.models import Competition
        
class DatabasePipeline(object):
    def process_item(self, item, spider):
        competition = item.save()
        Competition.objects.create(
                                   description=unicode(item),
                                   start_time=item['start_time'],
                                   end_time=item.end_time(),
                                   finished = item['finished'],
                                   tags = item.tags(),
                                   freeze_time = item.freeze_time(),
                                   competition_object = competition
                                   )
        return item
        
