# Scrapy settings for score_eater project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'score_eater'
BOT_VERSION = '1.0'

SPIDER_MODULES = ['score_eater.spiders']
NEWSPIDER_MODULE = 'score_eater.spiders'

DOWNLOADER_MIDDLEWARES = {
    'score_eater.middlewares.selenium_webdriver.SeleniumDownloader': 501,
}
ITEM_PIPELINES = [
    'score_eater.pipelines.DatabasePipeline',
]

def setup_django_env(path):
    import imp, os,sys
    from os.path import abspath, dirname, join
    from django.core.management import setup_environ

    f, filename, desc = imp.find_module('settings', [path])
    project = imp.load_module('settings', f, filename, desc)       

    setup_environ(project)
    from django.conf import settings
    sys.path.insert(0, join(settings.PROJECT_ROOT, "apps"))

setup_django_env('/home/dt/coding/doume/dowmer/')


USER_AGENT = '%s/%s' % (BOT_NAME, BOT_VERSION)

