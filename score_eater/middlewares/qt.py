from scrapy.http import Request, FormRequest, HtmlResponse
from django.utils.encoding import smart_str, smart_unicode
from score_eater.requests import InteractiveRequest

import sys
import signal
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *


class Crawler( QWebPage ):
    def __init__(self, url, file,click_list):
        self.app = QApplication( sys.argv )        
        QWebPage.__init__( self )
        self._url = url
        self._file = file
        self.click_list = click_list        
 
    def crawl( self ):
        signal.signal( signal.SIGINT, signal.SIG_DFL )
        self.connect( self, SIGNAL( 'loadFinished(bool)' ), self._finished_loading )
        self.mainFrame().load( QUrl( self._url ) )
        self.app.exec_()
 
    def _finished_loading( self, result ):
        print self.click_list
        if len(self.click_list)==0:
            print "finishing"
            file = open( self._file, 'w' )
            file.write( self.mainFrame().toHtml() )
            file.close()
            self.html = self.mainFrame().toHtml()
            self.app.quit()
        else:
            print "executing js"
            element = self.click_list.pop()                
            jscode = "pgenerate(true,7)"        
            self.mainFrame().evaluateJavaScript(jscode)
            print "done"
        

class WebkitDownloader( object ):
    
    def __init__(self):
        self.app = QApplication( sys.argv )
        self.web = QWebView()
        self.webpage = self.web.page()
        signal.signal( signal.SIGINT, signal.SIG_DFL )
        self.webpage.connect( self.webpage, SIGNAL( 'loadFinished(bool)' ), self._finished_loading )
        self.webpage.mainFrame().load( QUrl( "http://www.flashscore.com/soccer/france/" ) )
        self.web.show()    
            
    def _finished_loading( self, result ):
        print self.click_list
        if len(self.click_list)==0:
            self.
        else:
            print "executing js"
            element = self.click_list.pop()                
            jscode = "pgenerate(true,7)"        
            self.webpage.mainFrame().evaluateJavaScript(jscode)
            print "done"
    #expects an InteractiveRequest
    def process_request( self, request, spider ): 
         
        if( type(request) is not FormRequest ):            
            if( type(request) is InteractiveRequest ):                
                self.click_list = request.click_list
                
            else:
                self.click_list = []
            
            signal.signal( signal.SIGINT, signal.SIG_DFL )
            self.webpage.connect( self.webpage, SIGNAL( 'loadFinished(bool)' ), self._finished_loading )
            self.app.exec_()
            self.webpage.disconnect(self.webpage,SIGNAL( 'loadFinished(bool)'),self._finished_loading)
                        
            return HtmlResponse( request.url, body=smart_str(self.html) )
        
        