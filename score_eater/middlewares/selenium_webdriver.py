from scrapy.http import Request, FormRequest, HtmlResponse
from django.utils.encoding import smart_str, smart_unicode
from score_eater.requests import InteractiveRequest

from selenium import webdriver

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.common.by import By


class SeleniumDownloader( object ):
    def process_request( self, request, spider ):
        if( type(request) is not FormRequest ):
            #driver = webdriver.Remote("http://localhost:4444/wd/hub", webdriver.DesiredCapabilities.CHROME)
            driver = webdriver.Firefox()
            driver.get(request.url)  
            
            html = driver.page_source
            
            for click_element in spider.click_list:                    
                try:    
                    clickable_xpath = click_element.get("existing",None)
                    expected_xpath = click_element.get("expected",None)
                    if clickable_xpath is not None:
                        clickable = driver.find_element(By.XPATH,clickable_xpath)                        
                        clickable.click()
                    if expected_xpath is not None:
                        WebDriverWait(driver, 10).until(lambda driver : driver.find_element(By.XPATH,expected_xpath))
                    html = driver.page_source
                    
                finally:
                    pass
        
            driver.quit()
            return HtmlResponse( request.url, body=smart_str(html) )