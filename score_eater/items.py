# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib_exp.djangoitem import DjangoItem
from dooms.competitions.soccer.models import SoccerCompetition
from datetime import timedelta

class SoccerItem(DjangoItem):
    PENDING = 1
    HALF_TIME = 2
    FINISHED = 3 
    SCHEDULED = 4
    
    SOCCER_MATCH_DURATION = timedelta(minutes=90)
    
    finished = Field()    
    
    start_time = Field()
    
    def tags(self):
        return ' '.join(['foot',self['league'],self['side1'],self['side2']])
    
    def end_time(self):
        return self['start_time'] + self.SOCCER_MATCH_DURATION
    
    def freeze_time(self):
        return self['start_time'] + self.SOCCER_MATCH_DURATION - timedelta(minutes=10)

    def __unicode__(self):
        vs = self['side1'] + " vs " + self['side2']
        if self['finished']:                    
            return "[Finished] " + vs + " scores: " + unicode(self['score1']) + " - " + unicode(self['score2'])
        else:
            if self['status'] == self.PENDING:
                return "[Pending] " + vs + " scores: " + unicode(self['score1']) + " - " + unicode(self['score2'])
            elif self['status'] == self.SCHEDULED:
                return "[Scheduled] " + vs
    
    django_model = SoccerCompetition