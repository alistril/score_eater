from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
import string
import re
from score_eater.items import SoccerItem
from score_eater.requests import InteractiveRequest
from datetime import datetime, timedelta
import pytz


class SoccerSpider(BaseSpider):
    name = "soccer"
    allowed_domains = ["flashscore.com"]
    start_urls = ["http://www.flashscore.com/soccer/france/"]

    def __init__(self,*args,**kwargs):
        self.time_delta = None
        self.click_list=[
                            { "expected" : "//div[@id='fscon']/ul/li%s/span/a" % self._xpath_has_class("li2")}
                        ]
        super(SoccerSpider,self).__init__(*args,**kwargs)
        
    def _match_score(self,score,regex):
        res = re.match( regex, score)
        if res:
            score1 = int(res.group(1))
            score2 = int(res.group(2))
            
            return score1,score2
        else:
            return None,None
        
    def _extract_date(self,date_time):
        if self.time_delta:
            timezone = pytz.timezone('GMT')

            date_time = datetime.strptime(date_time,"%d.%m. %H:%M")
            date_time = date_time.replace(year=datetime.now().year)
            
            return timezone.normalize(timezone.localize(date_time)-timedelta(hours=self.time_delta))
            
    def _get_score_before(self,competition_line):
        score_before = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' score ')]/span[contains(concat(' ', normalize-space(@class), ' '), ' aet ')]/text()").extract()
        score1_before = None
        score2_before = None
        if len(score_before) == 1:
            score_before = score_before[0]
            score1_before,score2_before = self._match_score(score_before,r'[\ ]*\([\ ]*([0-9]+)[\ ]*\-[\ ]*([0-9]+)[\ ]*\)[\ ]*')                        
            
        return score1_before,score2_before
    

    def _xpath_has_class(self,classname):
        return "[contains(concat(' ', normalize-space(@class), ' '), ' %s ')]" % classname
    
        
    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        
        timezone = hxs.select("//*[@id='tzactual']/text()").extract()
        
        if len(timezone)==1:
            timezone = timezone[0]
            
            res = re.match(r'[\w\s\:\,\ \.\xa0]*GMT(\+[0-9]+)',timezone)
              
            if res:
                self.time_delta=int(res.group(1))
            
                league_tables = hxs.select("//div[@id='fscountry']/div[@id='fscon']/div[@id='fs']//table")
                
                for league_table in league_tables:            
                    league_line = league_table.select(".//tr[contains(@class, 'league')]")
                    
                    competition_lines = league_table.select(".//tr[not(contains(@class, 'league'))]")        
                
                    league_name = league_line.select(".//span[contains(@class, 'country')]//span[contains(@class, 'name')]/text()").extract()
                    if len(league_name) == 1:
                        league_name = league_name[0]
                        
                        league_name = re.sub('[^A-Za-z0-9\ ]+', ',', league_name)
                    
                        for competition_line in competition_lines:
                            #specify span classes (padl/padr)!
                            team_home = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' team-home ')]/span[contains(concat(' ', normalize-space(@class), ' '), ' padr ')]/text()").extract()
                            team_away = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' team-away ')]/span[contains(concat(' ', normalize-space(@class), ' '), ' padl ')]/text()").extract()
                            date_time = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' time ')]/text()").extract()
                            status = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' timer ')]/span/text()").extract()
                            score = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' score ')]/text()").extract()
                            score_half = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' part-top ')]/text()").extract()
                            
                            if len(team_home) ==1 and len(team_away) == 1 and len(score)==1 and len(date_time)==1 and len(status)==1:
                                team_home = team_home[0] 
                                team_away = team_away[0]                        
                                score = score[0]
                                date_time = date_time[0]
                                status = status[0]
                                
                                score1,score2 = self._match_score(score,r'[\ ]*([0-9]+)[\ ]*\-[\ ]*([0-9]+)[\ ]*')
                                
                                if score1 is not None and score2 is not None:                            
                                    item = SoccerItem()
                                    
                                    item['league'] = league_name
                                    
                                    item['score1'] = score1
                                    item['score2'] = score2
                                    
                                    item['side1'] = team_home
                                    item['side2'] = team_away                                    
                                    
                                    item['start_time'] = self._extract_date(date_time)
                                       
                                    re_after_extratime = re.compile(r'[\s\xa0]*[a-zA-Z0-9\.\ ]*After\sET[a-zA-Z0-9\.\ ]*[\s\xa0]*', re.IGNORECASE)
                                    re_after_penalty = re.compile(r'[\s\xa0]*[a-zA-Z0-9\.\ ]*After\sPen[a-zA-Z0-9\.\ ]*[\s\xa0]*', re.IGNORECASE)
                                    re_finished = re.compile(r'[\s\xa0]*[a-zA-Z0-9\.\ ]*Finished[a-zA-Z0-9\.\ ]*[\s\xa0]*', re.IGNORECASE)
                                    re_pending = re.compile(r'[\s\xa0]*[0-9]+[\s\xa0]*', re.IGNORECASE)
                                    re_half_time = re.compile(r'[\s\xa0]*[a-zA-Z0-9\.\ ]*Half\sTime[a-zA-Z0-9\.\ ]*[\s\xa0]*', re.IGNORECASE)
                                    
                                    if len(score_half)==1:
                                        score_half=score_half[0]
                                        score1_half,score2_half = self._match_score(score_half,r'[\ ]*\([\ ]*([0-9]+)[\ ]*\-[\ ]*([0-9]+)[\ ]*\)[\ ]*')
                                        item['score1_half_time'] = score1_half
                                        item['score2_half_time'] = score2_half
                                        item['half_time_finished'] = True
                                                              
                                    if re_after_extratime.match( status ):
                                        score1_before,score2_before = self._get_score_before(competition_line)
                                        item['finished'] = True
                                        item['half_time_finished'] = True
                                        item['score1_before_extra_time'] = score1_before
                                        item['score2_before_extra_time'] = score2_before
                                        item['status'] = SoccerItem.FINISHED
                                    elif re_after_penalty.match( status ):
                                        score1_before,score2_before = self._get_score_before(competition_line)
                                        item['finished'] = True
                                        item['half_time_finished'] = True
                                        item['score1_before_penalty'] = score1_before
                                        item['score2_before_penalty'] = score2_before                                        
                                        item['status'] = SoccerItem.FINISHED                                            
                                    elif re_finished.match( status ):
                                        item['finished'] = True
                                        item['half_time_finished'] = True
                                        item['status'] = SoccerItem.FINISHED
                                    elif re_pending.match( status ):
                                        item['finished'] = False
                                        item['status'] = SoccerItem.PENDING
                                    elif re_half_time.match(status): #useless, just for fun
                                        item['status'] = SoccerItem.HALF_TIME
                                        item['finished'] = False
                                        item['score1_half_time'] = score1
                                        item['score2_half_time'] = score2
                                        item['half_time_finished'] = True                                        
                                    else:
                                        continue
                                                                                    
                                    yield item  
                                   
        self.click_list = [
                              { "expected" : "//div[@id='fscon']/ul/li%s/span/a" % self._xpath_has_class("li2")},
                              {
                               "existing" : "//div[@id='fscon']/ul/li%s/span/a" % self._xpath_has_class("li2"),
                               "expected" : "//div[@id='fscon']/ul/li%s/span/a" % self._xpath_has_class("li1"),
                               }
                            ]
        yield Request("http://www.flashscore.com/soccer/france/",callback=self.parse_scheduled)        
    
    def parse_scheduled(self, response):
        hxs = HtmlXPathSelector(response)
        league_tables = hxs.select("//div[@id='fscountry']/div[@id='fscon']/div[@id='fs']//table")
        re_not_empty = re.compile(r'[\s\xa0]*[a-zA-Z0-9\.]+[\s\xa0]*', re.IGNORECASE)
        
        for league_table in league_tables:            
            league_line = league_table.select(".//tr[contains(@class, 'league')]")
            
            competition_lines = league_table.select(".//tr[not(contains(@class, 'league'))]")        
        
            league_name = league_line.select(".//span[contains(@class, 'country')]//span[contains(@class, 'name')]/text()").extract()
            if len(league_name) == 1:
                league_name = league_name[0]
                
                league_name = re.sub('[^A-Za-z0-9\ ]+', ',', league_name)
            
                for competition_line in competition_lines:
                    team_home = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' team-home ')]/span[contains(concat(' ', normalize-space(@class), ' '), ' padr ')]/text()").extract()
                    team_away = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' team-away ')]/span[contains(concat(' ', normalize-space(@class), ' '), ' padl ')]/text()").extract()
                    date_time = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' time ')]/text()").extract()
                    status = competition_line.select("td[contains(concat(' ', normalize-space(@class), ' '), ' timer ')]/span/text()").extract()
                    if len(team_home) ==1 and len(team_away) == 1 and len(date_time)==1 and len(status)==1:                        
                        team_home = team_home[0] 
                        team_away = team_away[0]
                        date_time = date_time[0]
                        status=status[0]

                        if re_not_empty.match(status) is None: #only scheduled (with no status)
                            item = SoccerItem()
                            
                            item['finished'] = False
                            item['status'] = SoccerItem.SCHEDULED
                            item['league'] = league_name
                            
                            item['side1'] = team_home
                            item['side2'] = team_away
                            item['start_time'] = self._extract_date(date_time)
    
                            yield item        
        
        