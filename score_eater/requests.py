from scrapy.http import Request

class InteractiveRequest(Request):
    def __init__(self,*args,**kwargs):
        self.click_list = kwargs.pop("click_list",None)
        super(InteractiveRequest, self).__init__(*args,**kwargs)
        


